// Soal Nomor 1
function arrayToObject(arr) {
    var result = {}
    for (var a = 0; a<arr.length; a++) {
        result.firstName = arr[a][0];
        result.lastName = arr[a][1];
        result.gender = arr[a][2];
        var now = new Date()
        var thisYear = now.getFullYear()
        if ((arr[a][3]>thisYear)|| arr[a][3]==null) {
            result.age = "Invalid Birth Year"
        } else {
            result.age = (thisYear) - (arr[a][3])
        }
        console.log((a+1)+". "+result.firstName+" "+result.lastName+":",result)
    }
}


// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""
console.log("")
// Soal nomor 2
function shoppingTime(memberId, money) {
    if ((memberId=="") || (memberId == null)) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else {
        if (money <50000) {
            return "Mohon maaf, uang tidak cukup"
        } else {
            var obj = {}
            var sisa_uang = money
            var barang = []
            var beli = true
            var beli_sepatu = true
            var beli_Zoro = true
            var beli_HN = true
            var beli_uq = true
            var beli_case = true
            while (beli==true) {
                if (sisa_uang < 50000 || beli_case==false ) {
                    beli = false
                } else {
                    if (sisa_uang>=1500000 && beli_sepatu==true) {
                        barang.push("Sepatu Stacattu")
                        beli_sepatu=false
                        sisa_uang = sisa_uang - 1500000
                    } if (sisa_uang>=500000 && beli_Zoro == true) {
                        barang.push("Baju Zoro")
                        beli_Zoro =false
                        sisa_uang = sisa_uang - 500000
                    } if (sisa_uang>=250000 && beli_HN == true) {
                        barang.push("Baju H&N")
                        beli_HN = false
                        sisa_uang = sisa_uang - 250000
                    } if (sisa_uang>=175000 && beli_uq == true) {
                        barang.push("Sweater Uniklooh")
                        beli_uq = false
                        sisa_uang = sisa_uang - 175000
                    } if (sisa_uang>=50000 && beli_case == true) {
                        barang.push("Casing Handphone")
                        beli_case = false
                        sisa_uang = sisa_uang - 50000
                    }
                }
            }
            obj.memberId = memberId
            obj.money = money
            obj.listPurchased = barang
            obj.changeMoney = sisa_uang
            return obj
        }
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  console.log("")
  // Soal nomor 3
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrObj = []
    for (var a = 0; a<arrPenumpang.length;a++) {
        var obj = {}
        var masuk = 0
        var keluar = 0
        obj.penumpang = arrPenumpang[a][0]
        obj.naikDari = arrPenumpang[a][1]
        obj.tujuan = arrPenumpang[a][2]
        switch(arrPenumpang[a][1]) {
            case "A" : {masuk =1;break}
            case "B" : {masuk =2;break}
            case "C" : {masuk =3;break}
            case "D" : {masuk =4;break}
            case "E" : {masuk =5;break}
            case "F" : {masuk =6;break}
        }
        switch(arrPenumpang[a][2]) {
            case "A" : {keluar =1;break}
            case "B" : {keluar =2;break}
            case "C" : {keluar =3;break}
            case "D" : {keluar =4;break}
            case "E" : {keluar =5;break}
            case "F" : {keluar =6;break}
        }
        bayaran = (keluar-masuk)*2000
        if (bayaran < 0) {
            bayaran = bayaran*(-1)
        }
        obj.bayar = bayaran
        arrObj.push(obj)
    } 
    return arrObj
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]
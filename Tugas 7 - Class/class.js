// Nomor 1 Animal Class
// Release 0
class Animal {
    constructor(name,legs) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
class Ape extends Animal {
    constructor(name,legs) {
        super(name,legs);
        this.legs = 2;   
    } 
    yell() {
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        console.log("hop hop")
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo";
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// Nomor 2 Function to Class
class Clock {
    constructor({template}) {
        this.template = template
    }
    render() {
        var date = new Date();
        var hours = date.getHours();       
        var mins = date.getMinutes();
        var secs = date.getSeconds();
        if (hours < 10) hours = '0' + hours;
        if (mins < 10) mins = '0' + mins;
        if (secs < 10) secs = '0' + secs;
        var output = this.template.replace('h', hours).replace('m', mins).replace('s', secs);

    console.log(output);
    }
    start() {
        this.render()
        var timer = setInterval(this.render.bind(this), 1000)
    }
    stop() {
        clearInterval(this.timer)
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  

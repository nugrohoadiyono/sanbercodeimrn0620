// Nomor 1 (Range)
function range(startNum="",finishNum="") {
    var array = []
    if ((startNum=="") || (finishNum=="")) {
        return -1
    } else {
        if(startNum<=finishNum) {for (var a = startNum; a<=(finishNum); a++) {
            array.push(a);}
        } else {
            for(var b = startNum; b>=(finishNum); b--) {array.push(b)}
        }
        return array
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("")

//Nomor 2 (Range with Step)
function rangeWithStep(startNum="", finishNum="", step) {
    var array = []
    if ((startNum=="") || (finishNum=="")) {
        return -1
    } else {
        if(startNum<=finishNum) {for (var a = startNum; a<=(finishNum); a+=step) {
            array.push(a);}
        } else {
            for(var b = startNum; b>=(finishNum); b-=step) {array.push(b)}
        }
        return array
    }
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("")

// Nomor 3 (Sum of Range)
function sum(startNum="", finishNum="", step="") {
    var jumlah = 0
    if (finishNum==""){if (startNum=="") {return jumlah} else {jumlah=startNum;return jumlah}}
    else {
        if (step=="") {step=1}
        if(startNum<=finishNum) {for (var a = startNum; a<=(finishNum); a+=step) {
            jumlah=jumlah+a}
        } else {
            for(var b = startNum; b>=(finishNum); b-=step) {jumlah=jumlah+b}
        }
        return jumlah
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("")

// Nomor 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
function dataHandling(masukan) {
    for(var a = 0; a <masukan.length; a++) {
        console.log("Nomor ID:  "+masukan[a][0]);
        console.log("Nama Lengkap:  "+masukan[a][1]);
        console.log("TTL:  "+masukan[a][2]+" "+masukan[a][3]);
        console.log("Hobi: "+masukan[a][4]);
        console.log("")
    }
}
dataHandling(input)

// Nomor 5 Balik Kata
function balikKata(kata) {
    var kembar = ""
    for(var a = (kata.length-1); a>=0;a--) {
        kembar+=kata[a]
    }
    return kembar
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("")

// Nomor 6 (Metode Array)
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(arr) {
    arr.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    arr.splice(4,1,"Pria","SMA Internasional Metro")
    console.log(arr)
    var ambil_bulan = arr[3]
    var tgl = ambil_bulan.split("/")
    var tgl_urut = ambil_bulan.split("/")
    switch (tgl[1]) {
        case "01": {console.log("Januari"); break;}
        case "02": {console.log("Februari"); break;}
        case "03": {console.log("Maret"); break;}
        case "04": {console.log("April"); break;}
        case "05": {console.log("Mei"); break;}
        case "06": {console.log("Juni"); break;}
        case "07": {console.log("Juli"); break;}
        case "08": {console.log("Agustus"); break;}
        case "09": {console.log("September"); break;}
        case "10": {console.log("Oktober"); break;}
        case "11": {console.log("November"); break;}
        case "12": {console.log("Desember"); break;}
        default: {break;}
    }
    tgl_urut.sort(function (value1, value2) { return value2 - value1 } ) ;
    console.log(tgl_urut)
    console.log(tgl.join("-"))
    var ambil_nama = arr[1]
    console.log(ambil_nama.slice(0,14))
}
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
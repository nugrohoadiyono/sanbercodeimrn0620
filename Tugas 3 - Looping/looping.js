// Nomor 1 - Looping While
var on = true
var angka = 1
var turun = false
while(on==true) {
    if(turun==false){
        if (angka == 1){
            console.log("LOOPING PERTAMA");
            angka++
        } else if ((angka >1) && (angka <=20)) {
            console.log(angka+" - I love coding");
            angka+=2;
            if (angka>20){
                turun = true
            }
        }
    } else if (turun==true) {
        if (angka>20) {
            if((angka-2)==20){
                console.log("LOOPING KEDUA"); 
            }
            angka-=2;           
        } else if ((angka<=20)&& (angka >= 2)) {
            console.log(angka+" - I will become a mobile developer");
            angka-=2;
        } else {
            on = false
        }
    }
}
console.log("")

// Nomor 2 Looping menggunakan for
for(var angka = 1; angka<=20; angka++){
    if ((angka%2)==0) {
        console.log(angka+" - Berkualitas");
    } else {
        if ((angka%3)==0) {
            console.log(angka+" - I Love Coding")
        } else {
            console.log(angka+" - Santai")
        }
    }
}
console.log("")

//Nomor 3 Membuat persegi panjang
var indeks = 1
var pagar1 = "#"
var buat = 1
while(indeks<=4) {
    while(buat<8){
        pagar1+= "#"
        buat++
    }
    console.log(pagar1)
    indeks++
}
console.log("")

//Nomor 4 Membuat Tangga
var index = 1
var pagar0 = ""
var row = 0
while (index<=7){
    while(row<index) {
        pagar0+="#"
        row++
    }
    console.log(pagar0)
    index++
}

console.log("")

//Nomor 5 Membuat Papan Catur
var idx = 1
var pagar = ""
var row1 = 0
while (idx<=8){
    while (row1<8){
        if (row1%2 == 0) {
            if(idx%2==1){
                pagar+=" "
            } else {
                pagar+="#"
            }
        } else {
            if(idx%2==1){
                pagar+="#"
            } else {
                pagar+=" "
            }
        }
        row1++
    }
    console.log(pagar)
    row1 = 0
    pagar = ""
    idx++
}
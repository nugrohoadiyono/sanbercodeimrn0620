// Nomor 1
const golden = () => {
    console.log("this is golden!!")
  }
   
  golden()
  

// Nomor 2
const newFunction = (firstName, lastName)=>{
    return{
      firstName,
      lastName,
      fullName(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
  //Driver Code 
  newFunction("William", "Imoh").fullName()

// Nomor 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const { firstName, lastName, destination, occupation, spell } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation)

//Nomor 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]

//Driver Code
console.log(combined)

// Nomor 5
const planet = "earth"
const view = "glass"
//var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
// Driver Code
console.log(before) 